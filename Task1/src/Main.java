import exceptionPackage.AgeLessThanException;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws AgeLessThanException {
        try {
            System.out.print("Enter your age: ");
            Scanner input = new Scanner(System.in);
            int age = input.nextInt();
            System.out.println("Your age is - " + age);
            ageCheck(age);
        } catch (InputMismatchException e) {
            System.out.println("Input Mismatch! Your age cannot start with letter!");
        }
    }

    private static void ageCheck (int age) throws AgeLessThanException  {
        if (age < 18) {
            throw new AgeLessThanException("Age is less than 18!");
        }
    }
}
