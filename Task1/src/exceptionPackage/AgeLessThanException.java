package exceptionPackage;

public class AgeLessThanException extends Throwable {
    public AgeLessThanException(){}
    public AgeLessThanException(String message) {
        super(message);
    }

    public AgeLessThanException(Throwable cause) {
        super(cause);
    }
}
